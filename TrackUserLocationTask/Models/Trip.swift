//
//  Trip.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import GoogleMaps

struct Trip {
    let id: Int
    let tripStart: Int32
    let tripEnd: Int32
    let tripStatus: Int
    let tripDetails: [TripDetail]?
}
