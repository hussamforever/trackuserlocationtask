//
//  TripSummary.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 04/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation

struct TripSummary {
    let dayName: String
    let startDateTime: String
    let endDateTime: String
    let duration: (Int, Int, Int)
    let durationFormatedString: String
}
