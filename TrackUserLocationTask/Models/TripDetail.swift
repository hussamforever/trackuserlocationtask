//
//  TripDetail.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 04/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation

// lat and lat are string becuase we encrypt them before save them into db

struct TripDetail {
    let id: Int
    let lat: String
    let lng: String
    
    init(id: Int, lat: String, lng: String) {
        self.id = id
        self.lat = lat
        self.lng = lng
    }
}
