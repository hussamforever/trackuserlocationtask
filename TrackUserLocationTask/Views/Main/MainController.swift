//
//  ViewController.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import GoogleMaps


class MainController: UIViewController {
    
    //MARK: Properties
    
    var circle: GMSCircle?
    var viewModel: MainViewModel!
    var tripPolylineList = [GMSPolyline]()
    var mapCurrentStyle = MapStyle.dayStyle
    
    lazy var mapView: CustomMapView = {
        let mapView = CustomMapView(frame: self.view.frame)
        return mapView
    }()
    
    lazy var trackingSwitchControl: UISwitch = {
        let switchControl = UISwitch(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        switchControl.isOn = false
        switchControl.onTintColor = .blue
        switchControl.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
        return switchControl
    }()
    
    lazy var recenterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "ic_gps").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(centerMapOnUserButtonClicked), for: .touchUpInside)
        return button
    }()
    
    lazy var styleMapButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "night_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(sytleButtonClicked), for: .touchUpInside)
        return button
    }()
    
    
    //MARK: Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        PersistenceManager.shared.setCurrentTripStatus(status: .notAvailable)
        setupNavBar()
        setupViews()
        setupViewsModels()
        viewModel.tryResumeTrip()
    }
}

//MARK: Privates

extension MainController {
    
    fileprivate func showLocationPermissionAlert() {
        presentAlertWithTitle(title: Captions.locationPermissionTitle, message: Captions.locationPermissionDenied, options: AlertOptions.yes.rawValue, AlertOptions.no.rawValue) { (option) in
            switch(option) {
            case AlertOptions.yes:
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
                break
            case AlertOptions.no:
                break
            }
        }
    }
    
    fileprivate func setupViewsModels() {
        viewModel = MainViewModel()
        viewModel.delegate = self
    }
    
    fileprivate func setupViews() {
        //MapView
        view.addSubview(mapView)
        mapView.addSubview(recenterButton)
        mapView.addSubview(styleMapButton)
        mapView.changeGoogleMapStyle(style: .dayStyle)
        
        //RecenterButton
        NSLayoutConstraint.activate([
            recenterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            recenterButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            recenterButton.widthAnchor.constraint(equalToConstant: 30),
            recenterButton.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        //styleMapButton
        NSLayoutConstraint.activate([
            styleMapButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            styleMapButton.topAnchor.constraint(equalTo: recenterButton.bottomAnchor, constant: 10),
            styleMapButton.widthAnchor.constraint(equalToConstant: 30),
            styleMapButton.heightAnchor.constraint(equalToConstant: 30)
            ])
    }
    
    fileprivate func setupNavBar() {
        navigationItem.title = Captions.trackEnabled
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Captions.myTrips, style: .plain, target: self, action: #selector(handleTripsClick))
        
        //Switch Button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: trackingSwitchControl)
    }
    
    @objc fileprivate func handleTripsClick() {
        navigationController?.pushViewController(TripsHistoryController(), animated: true)
    }
    
    @objc fileprivate func switchValueDidChange(sender: UISwitch!) {
        (sender.isOn) ? handleSwitchOn(): handleSwtichOff()
    }
    
    fileprivate func handleSwitchOn() {
        mapView.stopPolylinePathAnimation()
        tripPolylineList.removeAll()
        mapView.clearMap(tripPolylineList: tripPolylineList)
        mapView.isMyLocationEnabled = true
        viewModel.startNewTrip()
    }
    
    fileprivate func handleSwtichOff() {
        mapView.isMyLocationEnabled = false
        viewModel.endTrip()
        mapView.animatePolylinePath()
    }
    
    @objc fileprivate func centerMapOnUserButtonClicked() {
        viewModel.recenterToUserLocation()
    }
    
    @objc fileprivate func sytleButtonClicked() {
        if mapCurrentStyle == .dayStyle {
            styleMapButton.setImage(#imageLiteral(resourceName: "sun_icon").withRenderingMode(.alwaysOriginal), for: .normal)
            mapCurrentStyle = .nightStyle
            mapView.changeGoogleMapStyle(style: .nightStyle)
        } else {
            styleMapButton.setImage(#imageLiteral(resourceName: "night_icon").withRenderingMode(.alwaysOriginal), for: .normal)
            mapCurrentStyle = .dayStyle
            mapView.changeGoogleMapStyle(style: .dayStyle)
        }
    }
    
    fileprivate func updateNavigationTitle(title: String) {
        navigationItem.title = title
    }
    
    fileprivate func drawCircle(location: CLLocationCoordinate2D) {
        circle?.map = nil
        circle = GMSCircle(position: location, radius: CLLocationDistance(15))
        circle?.fillColor = UIColor.purple.withAlphaComponent(0.2)
        circle?.strokeColor = UIColor.purple.withAlphaComponent(0.7)
        circle?.strokeWidth = 2
        circle?.map = self.mapView
    }
}

//MARK: Callback

extension MainController: MainViewModelDelegate {
    func locationPermissionNotAvailable() {
        trackingSwitchControl.isOn = false
        showLocationPermissionAlert()
    }
    
    func moveToLocation(location: CLLocationCoordinate2D) {
        mapView.moveCamera(location: location)
    }
    
    func userDidMove(previousLocation: CLLocationCoordinate2D, headLocation: CLLocationCoordinate2D) {
        //draw polyline
        let polyline = mapView.plotPolyline(previousLocation: previousLocation, headLocation: headLocation)
        self.tripPolylineList.append(polyline)
        //Draw cicle around user
        drawCircle(location: headLocation)
    }
    
    func plotMarker(title: String, snippet: String, icon: UIImage, location: CLLocationCoordinate2D, isStartMarker: Bool) {
        mapView.plotMarker(location: location, isStartMarker: isStartMarker)
    }
    
    func initUI() {
        //Start Marker
        mapView.plotMarker(location: viewModel.tripLocations[0], isStartMarker: true)
                
        //Plot Polylines
        var i = 0;
        while i < viewModel.tripLocations.count {
            let fromLocation = viewModel.tripLocations[i]
            if i + 1 >= viewModel.tripLocations.count {
                break
            }
            let headLocation = viewModel.tripLocations[i + 1]
            let polyine = mapView.plotPolyline(previousLocation: fromLocation, headLocation: headLocation)
            tripPolylineList.append(polyine)
            i = i + 1
        }
        
        trackingSwitchControl.setOn(true, animated: true)
        mapView.isMyLocationEnabled = true
        
        //TODO Show snack bar message to user to inform trip has been
        //been resumed
       
        //Move camera to last location
        let lastLocation = viewModel.tripLocations[viewModel.tripLocations.count - 1]

        mapView.moveCamera(location: lastLocation)
    }
}
