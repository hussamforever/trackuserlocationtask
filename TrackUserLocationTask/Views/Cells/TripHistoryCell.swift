//
//  TripHistoryCell.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit

protocol TripHistoryCellDelegate: class {
    func handleDeleteTrip(trip: Trip)
    func handleViewTrip(trip: Trip)
}

class TripHistoryCell: UITableViewCell {
    var indexRow = 0
    
    /*
     In MVVM , some approaches do not allow to expose model in view,
     and other approaches do. I have decided to expose when only is very necessary
    */
    var trip: Trip? {
        didSet {
            guard let trip = trip else { return }
            tripNoLabelView.text = "Trip No \(trip.id)"
        }
    }
    
    //weak to avoid retain cycle
    weak var delegate: TripHistoryCellDelegate?
    
    let stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let tripNoLabelView : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "BebasNeueBold", size: 30)
        label.textColor = UIColor.white
        label.text = "Trip"
        label.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return label
    }()
    
    lazy var viewTripButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        button.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 30)
        button.titleLabel?.textColor = UIColor.white
        button.addTarget(self, action: #selector(handleViewTrip), for: .touchUpInside)
        button.setTitle("View Trip", for: .normal)
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return button
    }()
    
    lazy var deleteTripButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        button.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 30)
        button.titleLabel?.textColor = UIColor.white
        button.addTarget(self, action: #selector(handleDeleteTrip), for: .touchUpInside)
        button.setTitle("Delete", for: .normal)
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupViews(){
        addSubview(stackView)
        stackView.addArrangedSubview(tripNoLabelView)
        stackView.addArrangedSubview(viewTripButton)
        stackView.addArrangedSubview(deleteTripButton)
        //tripNoLabelView
        NSLayoutConstraint.activate([
                stackView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
                stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
                stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
                stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
            ])
    }

    @objc func handleViewTrip() {
        guard let trip = trip else { return }
        delegate?.handleViewTrip(trip: trip)
    }
    
    @objc func handleDeleteTrip() {
        guard let trip = trip else { return }
        delegate?.handleDeleteTrip(trip: trip)
    }
}
