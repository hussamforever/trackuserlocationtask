//
//  TripHistoryController.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 04/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import GoogleMaps

class TripHistoryDetailController: UIViewController {
    //MARK: Properties
    // In MVVM , some approaches do not allow to expose model in view, and other approaches
    
    var trip: Trip?
    var carMarker: GMSMarker!
    var viewModel = TripHistoryDetailViewModel()
    var carTimer: Timer!
    var mapCurrentStyle = MapStyle.dayStyle
    
    lazy var mapView: CustomMapView = {
        let mapView = CustomMapView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.bounds.height - 250))
        return mapView
    }()
    
    let stackViewVerticle: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let tripDayLabelView: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tuesday"
        label.textAlignment = .center
        return label
    }()
    
    let tripStartLabelView: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tripEndLabelView: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let durationLabelView: UILabel = {
        let label = UILabel()
        label.textColor = .green
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Duration: 20 Min"
        label.textAlignment = .center
        return label
    }()
    
    lazy var styleMapButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "night_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(sytleButtonClicked), for: .touchUpInside)
        return button
    }()
    
    //MARK: Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        viewModel.delegate = self
        viewModel.getTripDetail(trip: trip)
    }
    
    deinit {
        print("TripHistoryDetailController ended")
        if self.carTimer != nil {
            carTimer.invalidate()
            carTimer = nil
        }
    }
}

//MARK: Privates

extension TripHistoryDetailController {

    fileprivate func setupViews() {
        view.backgroundColor = .black
        
        //mapview
        view.addSubview(mapView)
        view.addSubview(stackViewVerticle)
        view.addSubview(styleMapButton)

        //stackViewVerticle
        stackViewVerticle.addArrangedSubview(tripDayLabelView)
        stackViewVerticle.addArrangedSubview(tripStartLabelView)
        stackViewVerticle.addArrangedSubview(tripEndLabelView)
        stackViewVerticle.addArrangedSubview(durationLabelView)
        stackViewVerticle.spacing = 12
        NSLayoutConstraint.activate([
            stackViewVerticle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            stackViewVerticle.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            stackViewVerticle.topAnchor.constraint(equalTo: mapView.bottomAnchor, constant: 20)
            ])
  
        //styleMapButton
        NSLayoutConstraint.activate([
            styleMapButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            styleMapButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            styleMapButton.widthAnchor.constraint(equalToConstant: 30),
            styleMapButton.heightAnchor.constraint(equalToConstant: 30)
            ])
    }
    
    fileprivate func animateCar() {
        var k = 0
        carMarker = mapView.plotCarMarker(location: viewModel.tripLocations[0])
        carTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {[weak self] (timer) in
            guard let selfRef = self else { return }
        
            if k > selfRef.viewModel.tripLocations.count - 2 {
                return
            }
            
            selfRef.mapView.animateCar(carMarker: selfRef.carMarker, oldCoordinate: selfRef.viewModel.tripLocations[k], newCoordinate: selfRef.viewModel.tripLocations[k + 1])
            k += 1
        }
    }
    
    fileprivate func plotPolylines() -> [GMSPolyline]{
        var i = 0;
        var tempPolylinelist = [GMSPolyline]()
        while i < viewModel.tripLocations.count {
            let fromLocation = viewModel.tripLocations[i]
            if i + 1 >= viewModel.tripLocations.count {
                break
            }
            let headLocation = viewModel.tripLocations[i + 1]
            let polyline = mapView.plotPolyline(previousLocation: fromLocation, headLocation: headLocation)
            tempPolylinelist.append(polyline)
            i += 1
        }
        
        return tempPolylinelist
    }
    
    @objc fileprivate func sytleButtonClicked() {
        if mapCurrentStyle == .dayStyle {
            styleMapButton.setImage(#imageLiteral(resourceName: "sun_icon").withRenderingMode(.alwaysOriginal), for: .normal)
            mapCurrentStyle = .nightStyle
            mapView.changeGoogleMapStyle(style: .nightStyle)
            carMarker.icon = UIImage(#imageLiteral(resourceName: "car_x"))
        } else {
            styleMapButton.setImage(#imageLiteral(resourceName: "night_icon").withRenderingMode(.alwaysOriginal), for: .normal)
            mapCurrentStyle = .dayStyle
            mapView.changeGoogleMapStyle(style: .dayStyle)
            carMarker.icon = UIImage(#imageLiteral(resourceName: "car_black"))
        }
    }
}

//MARK: TripHistoryDetailDelegate

extension TripHistoryDetailController: TripHistoryDetailDelegate {
    
    func initUI() {

        let fromLocation = viewModel.tripLocations[0]
        let toLocation = viewModel.tripLocations[viewModel.tripLocations.count - 1]
        
        //Car Marker Animation
        animateCar()
        
        //Start Marker
        mapView.plotMarker(location: fromLocation, isStartMarker: true)
        
        //End Marker
        mapView.plotMarker(location: toLocation, isStartMarker: false)
        
        //Populate trip info
        tripDayLabelView.text = viewModel.tripSummary?.dayName
        tripStartLabelView.text = viewModel.tripSummary?.startDateTime
        tripEndLabelView.text = viewModel.tripSummary?.endDateTime
        durationLabelView.text = viewModel.tripSummary?.durationFormatedString
        
        //Move Camera to region
        mapView.moveCameraToRegion(fromLocation: fromLocation, toLocation: toLocation, polylineList: plotPolylines())
    }
}
