//
//  TripsHistoryController.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit

class TripsHistoryController: UITableViewController, TripsHistoryDelegate {
  
    //MARK: Properties
    
    fileprivate let cellId = "cellId"
    fileprivate var viewModel = TripsHistoryViewModel()
    
    //MARK: Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(TripHistoryCell.self, forCellReuseIdentifier: cellId)
        viewModel.delegate = self
        viewModel.getTrips()
    }
    
    deinit {
        print("TripsHistoryController ended");
    }
}

//MARK: Callbacks

extension TripsHistoryController {
    func reloadTable() {
        self.tableView.reloadData()
    }
    
    func goToTripDetailController(trip: Trip) {
        let tripHistoryDetailController = TripHistoryDetailController()
        tripHistoryDetailController.trip = trip
        navigationController?.pushViewController(tripHistoryDetailController, animated: true)
    }
}

// MARK: - UITableView Delegate And Datasource Methods

extension TripsHistoryController: TripHistoryCellDelegate {
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TripHistoryCell
        cell.trip = viewModel.trips[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.trips.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    //Cell callback from cell
 
    func handleDeleteTrip(trip: Trip) {
        presentAlertWithTitle(title: Captions.deleteTitle, message: Captions.deleteTripCofirmation, options: AlertOptions.yes.rawValue, AlertOptions.no.rawValue) { [weak self] (option) in
            switch(option) {
            case AlertOptions.yes:
                self?.viewModel.deleteTrip(tripId: trip.id)
                break
            case AlertOptions.no:
                break
            }
        }
    }
    
    func handleViewTrip(trip: Trip) {
        let tripDetailController = TripHistoryDetailController()
        tripDetailController.trip = trip
        navigationController?.pushViewController(tripDetailController, animated: true)
    }
}

