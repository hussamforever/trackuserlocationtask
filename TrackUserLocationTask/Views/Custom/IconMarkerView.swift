//
//  PickupMarkerView.swift
//  Uber
//
//  Created by Husam Abuhajjaj on 17/08/2019.
//  Copyright © 2019 Husam Abuhajjaj. All rights reserved.
//

import UIKit

class IconMarkerView: UIView {
    
    lazy var iconLabelView: UILabel = {
        let labelView = UILabel()
        labelView.translatesAutoresizingMaskIntoConstraints = false
        labelView.textAlignment = .center
        labelView.backgroundColor = .white
        labelView.numberOfLines = 0
        labelView.font = UIFont.boldSystemFont(ofSize: 16)
        labelView.text = "Start Location"
        return labelView
    }()
    
    let iconMarkerImageView: UIImageView = {
       let imageView = UIImageView(image: #imageLiteral(resourceName: "floow_ic_pin_from"))
       imageView.contentMode = .scaleAspectFit
       imageView.translatesAutoresizingMaskIntoConstraints = false
       imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
       return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    fileprivate func setupViews() {
        let stackViewAll = UIStackView(arrangedSubviews: [
            iconLabelView,
            iconMarkerImageView
            ])
        
        stackViewAll.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        stackViewAll.backgroundColor = .blue
        stackViewAll.axis = .vertical
        stackViewAll.distribution = .fillProportionally
   
        addSubview(stackViewAll)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc fileprivate func handlePickupClick() {
        print("handlePickup")
    }
}
