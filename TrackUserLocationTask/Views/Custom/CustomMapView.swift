//
//  CustomMapView.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 06/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import GoogleMaps

class CustomMapView: GMSMapView {
    var timer: Timer!
    var polylineLocations = [CLLocationCoordinate2D]()
    var arcCarMovement = ARCarMovement()
 
    override init(frame: CGRect) {
        super.init(frame: frame)

        camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: 53.462394, longitude: -2.249880), zoom: MapOptions.zoom)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func plotMarker(location: CLLocationCoordinate2D, isStartMarker: Bool) {
        let marker = GMSMarker()
        marker.position = location
        marker.map = self

        if isStartMarker {
           let iconView = IconMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
           iconView.iconMarkerImageView.image = #imageLiteral(resourceName: "floow_ic_pin_from")
           marker.iconView = iconView
        } else {
           let iconView = IconMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
           iconView.iconMarkerImageView.image = #imageLiteral(resourceName: "floow_ic_pin_to")
           iconView.iconLabelView.backgroundColor = .black
           iconView.iconLabelView.textColor = .white
           iconView.iconLabelView.text = Captions.endLocation
           marker.iconView = iconView
        }
    }
    
    func moveCamera(location: CLLocationCoordinate2D) {
        animate(toLocation: location)
    }
    
    func clearMap(tripPolylineList: [GMSPolyline]) {
        for polyline in tripPolylineList {
            polyline.map = nil
        }
        clear()
    }
    
    func plotPolyline(previousLocation: CLLocationCoordinate2D, headLocation: CLLocationCoordinate2D) -> GMSPolyline {
        let path = GMSMutablePath()
        path.add(previousLocation)
        path.add(headLocation)
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.blue.withAlphaComponent(0.8)
        polyline.strokeWidth = 6.0
        polyline.map = self
        polylineLocations.append(previousLocation)
        polylineLocations.append(headLocation)
        return polyline
    }
    
    func plotTrip(tripLocations: [CLLocationCoordinate2D]) {
        //Start Marker
        plotMarker(location: tripLocations[0], isStartMarker: true)
        
        //End Marker
        plotMarker(location: tripLocations[tripLocations.count - 1], isStartMarker: false)
        
        var i = 0;
        
        while i < tripLocations.count {
            let fromLocation = tripLocations[i]
            if i + 1 >= tripLocations.count {
                break
            }
            
            let headLocation = tripLocations[i + 1]
            _ = plotPolyline(previousLocation: fromLocation, headLocation: headLocation)
            i = i + 1
        }
    }
    
    func moveCameraToRegion(fromLocation: CLLocationCoordinate2D?, toLocation: CLLocationCoordinate2D?, polylineList: [GMSPolyline]) {
            var bounds = GMSCoordinateBounds()
            if let fromLocation = fromLocation {
                bounds = bounds.includingCoordinate(fromLocation)
            }
            if let toLocation = toLocation {
                bounds = bounds.includingCoordinate(toLocation)
            }
            for polyline in polylineList {
                if let path = polyline.path {
                    bounds = bounds.includingPath(path)
                }
            }
            setMinZoom(1, maxZoom: 25)

            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 150, left: 60, bottom: 60, right: 70))
            animate(with: update)
    }
    
    func changeGoogleMapStyle(style: MapStyle) {
        do {
            if let styleURL = Bundle.main.url(forResource: style.rawValue, withExtension: "json") {
                mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch let error {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func stopPolylinePathAnimation() {
        if self.timer != nil {
            polylineLocations.removeAll()
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func animatePolylinePath() {
        let animationPolyline = GMSPolyline()
        var animationPath = GMSMutablePath()
        var i = 0
        stopPolylinePathAnimation()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 0.0009, repeats: true) { [weak self] (_) in
            guard let selfRef = self else {
                self?.stopPolylinePathAnimation()
                return
            }
            
            if (i < selfRef.polylineLocations.count) {
                animationPath.add(selfRef.polylineLocations[i])
                animationPolyline.path = animationPath
                animationPolyline.strokeColor = UIColor.black
                animationPolyline.strokeWidth = 3
                
                animationPolyline.map = selfRef
                i += 1
            }
            else {
                i = 0
                animationPath = GMSMutablePath()
                animationPolyline.map = nil
                //self.timer.invalidate()
            }
        }
    }
    
    //Car Animation
    
    func plotCarMarker(location: CLLocationCoordinate2D) -> GMSMarker {
        let marker = GMSMarker()
        marker.position = location
        marker.map = self
        marker.icon = #imageLiteral(resourceName: "car_black")
        return marker
    }
    
    func animateCar(carMarker: GMSMarker, oldCoordinate: CLLocationCoordinate2D, newCoordinate: CLLocationCoordinate2D) {
        arcCarMovement.arCarMovement(marker: carMarker, oldCoordinate: oldCoordinate, newCoordinate: newCoordinate, mapView: self)
    }
}


