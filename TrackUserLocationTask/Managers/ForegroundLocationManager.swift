//
//  LocationManager.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 05/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import CoreLocation

protocol ForegroundLocationManagerDelegate {
    func userDidMove(location: CLLocation)
}

class ForegroundLocationManager: BaseLocationManager {
    var delegate: ForegroundLocationManagerDelegate?
    
    func getCurrentLocation() -> CLLocation? {
        guard let currentLocation = locationManager.location else { return nil}
        return currentLocation
    }
    
    //MARK: Location Manager Call Back
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else { return }
        
        delegate?.userDidMove(location: currentLocation)
    }
    
}
