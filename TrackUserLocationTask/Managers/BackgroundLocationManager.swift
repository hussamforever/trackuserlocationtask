//
//  BackgroundLocationManager.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 06/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import CoreLocation

class BackgroundLocationManager: BaseLocationManager {
    
    let databaseManager = DatabaseManager()
    let backgroundManager = CLLocationManager()

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else { return }
        
        //Get saved current trip id
        let tripId = PersistenceManager.shared.getCurrentTripId()
        
        //Do nothing as trip ended
        if PersistenceManager.shared.getCurrentTripStatus() != .active {
            stopListening()
            return
        }
        
        //Encrypt locations before saving to db
        let encryptedLat = currentLocation.coordinate.latitude.toEncryptString()
        let encryptedLng = currentLocation.coordinate.longitude.toEncryptString()

        //Log into device for debug purposes
        locationLogger.writeLocationToFile(location: currentLocation, error: nil)
        
        //save into db
        databaseManager.insertTripDetail(tripId: tripId, lat: encryptedLat, lng: encryptedLng)
      
    }
    
}
