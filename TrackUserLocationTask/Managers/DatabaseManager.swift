//
//  DBManager.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import SQLite3

class DatabaseManager {

    let dbPath: String = "trackUserDB.sqlite"
    var db:OpaquePointer?
    
    init() {
        db = openDatabase()
        createAllTables()
    }
    
    //MARK: Open database and create tables
    
    func openDatabase() -> OpaquePointer? {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
            return nil
        }
        else {
            print("Successfully opened connection to database at \(dbPath)")
            return db
        }
    }
    
    func createAllTables() {
        //Create trip header table
        var sql = "CREATE TABLE IF NOT EXISTS trip(id INTEGER PRIMARY KEY AUTOINCREMENT, tripStart TIMESTAMP, tripEnd TIMESTAMP, tripStatus INTEGER DEFAULT 0);"
        createTable(sql: sql)
        
        //Create trip details table
        sql = "CREATE TABLE IF NOT EXISTS tripDetail(id INTEGER PRIMARY KEY AUTOINCREMENT, lat TEXT, lng TEXT, tripId INTEGER);"
        createTable(sql: sql)
    }
    
    func createTable(sql: String) {
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, sql, -1, &createTableStatement, nil) == SQLITE_OK {
           sqlite3_step(createTableStatement)
        }
        sqlite3_finalize(createTableStatement)
    }
    
    //MARK: Trip Table CRUD
    
    func createTrip(tripStartTimeStamp: Int32, tripEndTimeStamp: Int32) -> Int {
        let insertStatementString = "INSERT INTO trip (tripStart, tripEnd) VALUES (?, ?);"
        
        var insertStatement: OpaquePointer? = nil
        var tripId = 0
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_double(insertStatement, 1, Double(tripStartTimeStamp))
            sqlite3_bind_double(insertStatement, 2, Double(tripEndTimeStamp))
    
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                tripId = Int(sqlite3_last_insert_rowid(db))
            }
        }
        sqlite3_finalize(insertStatement)
        return tripId
    }
    
    func updateTrip(tripEndTimeStamp: Int32, tripStatus: TripStatus, id: Int) {
        let updateStatementString = "UPDATE trip set tripEnd = ?, tripStatus = ? WHERE id = ?;"
        
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            
            sqlite3_bind_double(updateStatement, 1, Double(tripEndTimeStamp))
            
            sqlite3_bind_int(updateStatement, 2, Int32(tripStatus.rawValue))
            
            sqlite3_bind_int(updateStatement, 3, Int32(id))
            
            sqlite3_step(updateStatement)
        }
        sqlite3_finalize(updateStatement)
    }
    
    func getAllTrips() -> [Trip] {
        let queryStatementString = "SELECT * FROM trip WHERE tripStatus = ?"
        var queryStatement: OpaquePointer? = nil
        var psns : [Trip] = []
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(TripStatus.complated.rawValue))
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = sqlite3_column_int(queryStatement, 0)
                
                let startTimestamp = sqlite3_column_double(queryStatement, 1)
                let endTimestamp = sqlite3_column_double(queryStatement, 2)
                let tripStatus = sqlite3_column_int(queryStatement, 3)

                let trip = Trip(id: Int(id),
                                tripStart: Int32(startTimestamp),
                                tripEnd: Int32(endTimestamp), tripStatus: Int(tripStatus), tripDetails: nil)
                
                psns.append(trip)
            }
        }
        sqlite3_finalize(queryStatement)
        return psns
    }
    
    func getTripDetails(tripId: Int) -> [TripDetail] {
        let queryStatementString = "SELECT * FROM tripDetail WHERE tripId = ? order by id"
        var queryStatement: OpaquePointer? = nil
        var psns : [TripDetail] = []
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(tripId))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let latString = String(cString: sqlite3_column_text(queryStatement, 1))
                let lngString = String(cString: sqlite3_column_text(queryStatement, 2))
                let trip = TripDetail(
                    id: Int(sqlite3_column_int(queryStatement, 0)),
                    lat: latString,
                    lng: lngString
                )
    
                psns.append(trip)
            }
        }
        sqlite3_finalize(queryStatement)
        return psns
    }
    
    func deleteTripById(id: Int) {
        //Delete trip header
        let deleteStatementStirng = "DELETE FROM trip WHERE id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(id))
            sqlite3_step(deleteStatement)
        }
        sqlite3_finalize(deleteStatement)
        
        //Delete Trip Details
        deleteTripDetailsByTripId(tripId: id)
    }
    
    func deleteTripDetailsByTripId(tripId: Int) {
        let deleteStatementStirng = "DELETE FROM tripDetail WHERE tripId = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(tripId))
            sqlite3_step(deleteStatement)
        }
        sqlite3_finalize(deleteStatement)
    }
    
    func insertTripDetail(tripId: Int, lat: String, lng: String) {
        let insertStatementString = "INSERT INTO tripDetail (tripId, lat, lng) VALUES (?, ?, ?);"
        
        var insertStatement: OpaquePointer? = nil
        let SQLITE_TRANSIENT = unsafeBitCast(OpaquePointer(bitPattern: -1), to: sqlite3_destructor_type.self)
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(insertStatement, 1, Int32(tripId))
            sqlite3_bind_text(insertStatement, 2, lat, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(insertStatement, 3, lng, -1, SQLITE_TRANSIENT)
        
            if sqlite3_step(insertStatement) == SQLITE_DONE {
            }
        }
        sqlite3_finalize(insertStatement)
    }
}
