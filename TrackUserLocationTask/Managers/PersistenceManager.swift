//
//  PersistenceManager.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit


class PersistenceManager {
    
    let currentTripId = "currentTripId"
    let tripStatus = "tripStatus"
    
    static let shared = PersistenceManager()
    let preferences = UserDefaults.standard
    
    init(){}
    
    func getCurrentTripId() -> Int {
        return preferences.integer(forKey: currentTripId)
    }
    
    func setCurrentTripId(tripId: Int){
        preferences.set(tripId, forKey: currentTripId)
    }
    
    func setCurrentTripStatus(status: TripStatus){
        preferences.set(status.rawValue, forKey: tripStatus)
    }
    
    func getCurrentTripStatus() -> TripStatus {
        let status = preferences.integer(forKey: tripStatus)
        return TripStatus(rawValue: status) ?? TripStatus.notAvailable
    }
}
