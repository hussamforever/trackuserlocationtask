//
//  BaseLocationManager.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 08/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import CoreLocation


class BaseLocationManager: NSObject, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    var previousLocation: CLLocation?
    var locationLogger = LocationLogger()
    
    override init() {
        super.init()
        setupLocationManager()
    }
    
    func startListening() {
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func stopListening() {
        locationManager.stopUpdatingLocation()
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    fileprivate func setupLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.activityType = .automotiveNavigation
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
    
    func isLocationServicesEnabled() -> Bool {
        let status = CLLocationManager.authorizationStatus()
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            return true
        } else {
            return false
        }
    }
    
    //    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    //        switch CLLocationManager.authorizationStatus() {
    //        case .authorizedAlways:
    //            print("I got authorized Always")
    //        case .authorizedWhenInUse:
    //            print("I got when in Use")
    //        case .notDetermined:
    //            locationLogger.writeLocationToFile(location: nil, error: "notDetermined")
    //            print("I not determin")
    //        case .restricted:
    //            locationLogger.writeLocationToFile(location: nil, error: "restricted")
    //        case .denied:
    //            locationLogger.writeLocationToFile(location: nil, error: "denied")
    //        @unknown default:
    //            locationLogger.writeLocationToFile(location: nil, error: "get error")
    //        }
    //    }
    
}
