//
//  DateExtensions.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
