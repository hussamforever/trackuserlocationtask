//
//  StringExtensions.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 05/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import CryptoSwift

//Ref:  https://github.com/krzyzanowskim/CryptoSwift

extension String {
   
    func aesEncrypt(key: String, iv: String) throws -> String {
        let encrypted = try AES(key: key, iv: iv, padding: .pkcs7).encrypt([UInt8](self.data(using: .utf8)!))
        return Data(encrypted).base64EncodedString()
    }
    
    func aesDecrypt(key: String, iv: String) throws -> String {
        guard let data = Data(base64Encoded: self) else { return "" }
        let decrypted = try AES(key: key, iv: iv, padding: .pkcs7).decrypt([UInt8](data))
        return String(bytes: decrypted, encoding: .utf8) ?? self
    }
    
    
}
