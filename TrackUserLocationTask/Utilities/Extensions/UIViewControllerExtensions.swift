//
//  UIViewController+AlertMessage.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAlertWithTitle(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: title, style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (AlertOptions) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                index == 0 ? completion(.yes): completion(.no)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}
