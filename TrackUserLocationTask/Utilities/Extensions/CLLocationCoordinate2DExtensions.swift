//
//  CLLocationCoordinate2DExtensions.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 07/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    func toCLLocation() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}
