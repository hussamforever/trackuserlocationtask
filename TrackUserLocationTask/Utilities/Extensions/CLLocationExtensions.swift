//
//  CLLocationExtensions.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import CoreLocation

extension CLLocation {
    
    func toCLLocationCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }

}
