//
//  CLLocationDegreesExtensions.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 08/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import CoreLocation


extension CLLocationDegrees {
    
    func toEncryptString() -> String {
        do {
            return try String(self).aesEncrypt(key: Security.key, iv: Security.iv)
        } catch _ {
            return ""
        }
    }
}
