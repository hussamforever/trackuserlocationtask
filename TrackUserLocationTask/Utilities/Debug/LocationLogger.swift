//
//  LocationLogger.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 06/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationLogger {
    private static var fileUrl = { () -> URL in
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        
        return fileURL.appendingPathComponent("locationTest.log")
    }()
    
    func removeLogFile() {
        try? FileManager.default.removeItem(at: LocationLogger.fileUrl)
    }
    
    func writeLocationToFile(location: CLLocation?, error: String?) {
        var string = ""
        if let location = location {
            string = "\(NSDate())&\(location.coordinate.latitude)&\(location.coordinate.longitude): Background\n"
        } else {
            string = error!
        }
        
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        if FileManager.default.fileExists(atPath: LocationLogger.fileUrl.path) {
            let fileHandle = try! FileHandle(forWritingTo: LocationLogger.fileUrl)
            fileHandle.seekToEndOfFile()
            fileHandle.write(data)
            fileHandle.closeFile()
            
        } else {
            try! data.write(to: LocationLogger.fileUrl)
        }
    }
    
    func readLocation() -> [CLLocation]? {
        do {
            let data = try String(contentsOf: LocationLogger.fileUrl)
            let locationStrings = data.components(separatedBy: .newlines)
            
            var locations:[CLLocation] = []
            
            locationStrings.forEach({ locString in
                let coordinateString = locString.split(separator: "&")
                if coordinateString.count > 2 {
                    let lat = Double(coordinateString[1])
                    let long = Double(coordinateString[2])
                    
                    locations.append(CLLocation(latitude: lat!, longitude: long!))
                }
            })
            
            return locations
        }
        catch {
            return nil
        }
    }
}
