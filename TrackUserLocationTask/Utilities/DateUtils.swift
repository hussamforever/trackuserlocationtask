//
//  CommonUtils.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 04/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation

//TODO : Husam create extensions
class DateUtils {
    
    static func timestampToDateTime(timestamp: Int32) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = DateTimeFormat.ukFormat
        let newDateTime = Date(timeIntervalSince1970: TimeInterval(timestamp))
      
        return dateFormatterGet.string(from: newDateTime)
    }
    
    static func dateToTimestamp(date: Date) -> Int32 {
        return Int32(date.timeIntervalSince1970)
    }

    static func getDayFromTimestamp(timestamp: Int32) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date)
    }
    
    static func timeDiff(startTime: Int32, endTime: Int32) -> Int {
        let time1 = Date(timeIntervalSince1970: TimeInterval(startTime))
        let time2 = Date(timeIntervalSince1970: TimeInterval(endTime))
        let difference = Calendar.current.dateComponents([.second], from: time1, to: time2)
        
        guard let duration = difference.second else { return 0 }
        
        return duration
    }
    
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
