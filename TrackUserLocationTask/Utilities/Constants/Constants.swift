//
//  Constants.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit

//Mark: Format

struct DateTimeFormat {
    static let ukFormat = "yyyy-MM-dd HH:mm:ss"
}

//Mark: Captions

struct Captions {
    static let trackDisabled = "Tracking Disabled"
    static let trackEnabled = "Tracking Enabled"
    static let myTrips = "My Trips"
    static let startLocation = "Start Location"
    static let endLocation = "End Location"
    static let tripStartAt = "Trip start at"
    static let tripEndAt = "Trip start at"
    static let tripResumed = "Trip has been resumed"
    static let locationPermissionTitle = "Location Permission Request"
    static let locationPermissionDenied = "To continue please enable location permission"
    static let deleteTitle = "Delete"
    static let deleteTripCofirmation = "Do you want to delete this trip?"
    
}

//MARK: Alert Message Options

enum AlertOptions: String {
    case yes = "Yes"
    case no = "No"
}

//=========          Enums    ==========   //

//MARK: Trip Status

enum TripStatus: Int {
    case notAvailable = 0
    case active = 1
    case complated = 2
}

//MARK: Key Chain

struct Security {
    static let googleMapKey = "AIzaSyAyi90C_nneYyDQYDCU_vh8V3hq42k9e8Y"
    static let key = "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw" // length == 32
    static let iv = "gqLOHUioQ0QjhuvI" // length == 16
}

//MARK: Maps

enum MapStyle: String {
    case dayStyle = "SilverStyle"
    case nightStyle = "NightStyle"
}

struct MapOptions {
    static let zoom: Float = 18.0
}
