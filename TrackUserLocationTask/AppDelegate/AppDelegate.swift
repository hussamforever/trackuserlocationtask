//
//  AppDelegate.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 02/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var backgroundLocationManager = BackgroundLocationManager()
    var locationLogger = LocationLogger()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Main Controller as root controller
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: MainController())
        
        //Google Map Register
        GMSServices.provideAPIKey(Security.googleMapKey)

        //Stop background manager because we are in forground
        backgroundLocationManager.stopListening()
        
        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
            //We only listen to trip if there is active trip
            if PersistenceManager.shared.getCurrentTripStatus() == TripStatus.active {
                backgroundLocationManager.startListening()
            }
        }

        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if PersistenceManager.shared.getCurrentTripStatus() == TripStatus.active {
            backgroundLocationManager.startListening()
        }
    }

}

