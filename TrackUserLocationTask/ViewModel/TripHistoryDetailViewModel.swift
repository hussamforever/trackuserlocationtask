//
//  TripHistoryDetailViewModel.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 04/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import GoogleMaps

protocol TripHistoryDetailDelegate: class {
    func initUI()
}

class TripHistoryDetailViewModel: BaseViewModel {
    var tripDetails:[TripDetail] = []
    let databaseManager = DatabaseManager()
    
    //weak to avoid retain cycle
    weak var delegate: TripHistoryDetailDelegate?
    var tripLocations = [CLLocationCoordinate2D]()
    var tripSummary: TripSummary?
    
    func getTripDetail(trip: Trip?) {
        guard let trip = trip else { return }
     
        tripDetails = databaseManager.getTripDetails(tripId: trip.id)
     
        let dayName = DateUtils.getDayFromTimestamp(timestamp: trip.tripStart)
        
        let startDateTime = "\(Captions.tripStartAt) : \(DateUtils.timestampToDateTime(timestamp: trip.tripStart))"
        
        let endDateTime = "\(Captions.tripEndAt) : \(DateUtils.timestampToDateTime(timestamp: trip.tripEnd))"
        
        let durationInSeconds = DateUtils.timeDiff(startTime: trip.tripStart, endTime: trip.tripEnd)
        
        let duration = DateUtils.secondsToHoursMinutesSeconds(seconds: durationInSeconds)
        
        let formatedDurationString = "H: \(duration.0) M: \(duration.1) S: \(duration.2)"
    
        tripSummary = TripSummary(dayName: dayName, startDateTime: startDateTime, endDateTime: endDateTime, duration: duration, durationFormatedString: formatedDurationString)
        
        createPolylineList()
        delegate?.initUI()
    }
    
    fileprivate func createPolylineList() {
        for tripDetail in self.tripDetails {
            let location = decryptLocation(lat: tripDetail.lat, lng: tripDetail.lng)
            tripLocations.append(location)
        }
    }
}
