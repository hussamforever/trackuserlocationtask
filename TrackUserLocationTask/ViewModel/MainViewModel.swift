//
//  MainViewModel.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import GoogleMaps

protocol MainViewModelDelegate {
    func moveToLocation(location: CLLocationCoordinate2D)
    func userDidMove(previousLocation: CLLocationCoordinate2D, headLocation: CLLocationCoordinate2D)
    func plotMarker(title: String, snippet: String, icon: UIImage, location: CLLocationCoordinate2D, isStartMarker: Bool)
    func initUI();
    func locationPermissionNotAvailable()
}

class MainViewModel: BaseViewModel {
    var databaseManager: DatabaseManager = DatabaseManager()
    var foregroundLocationManager = ForegroundLocationManager()
    var previousLocation: CLLocation?
    let zoomLevel: Float = 18.0
    let defaultLocation = CLLocation(latitude: 53.462394, longitude: -2.249880)
    var delegate: MainViewModelDelegate!
    var tripId = 0 // no trip
    var tripLocations = [CLLocationCoordinate2D]()
    
    override init() {
        super.init()
        foregroundLocationManager.delegate = self
    }
}

//MARK: Actions

extension MainViewModel {
    
    func startNewTrip() {
        //1 - Check locations permissions before start trip
        if foregroundLocationManager.isLocationServicesEnabled() == false {
            delegate?.locationPermissionNotAvailable()
            return
        }
        
        //2 - Create new trip
        self.tripId = databaseManager.createTrip(
            tripStartTimeStamp: DateUtils.dateToTimestamp(date: Date()),
            tripEndTimeStamp: 0 // will be updated when end trip
        )
        
        //3 - Save trip id & trip status into device
        PersistenceManager.shared.setCurrentTripId(tripId: self.tripId)
        PersistenceManager.shared.setCurrentTripStatus(status: .active)
        
        //4 - Move camera to user location
        if let currentLocation = foregroundLocationManager.getCurrentLocation() {
            delegate?.moveToLocation(location: currentLocation.toCLLocationCoordinate2D())
        }
        
        //5 - Enable/Start Location Tracking
        foregroundLocationManager.startListening()
    }
    
    func endTrip() {
        //There is not trip avaiable to end
        if PersistenceManager.shared.getCurrentTripStatus() != .active {
            return
        }
        
        //Set trip status to complated
        PersistenceManager.shared.setCurrentTripStatus(status: .complated)
        
        //Stop Listeing
        foregroundLocationManager.stopListening()
        
        //Update trip end time in database
        databaseManager.updateTrip(tripEndTimeStamp: DateUtils.dateToTimestamp(date: Date()), tripStatus: .complated, id: tripId)
        
        //Set current trip to 0 which means that no current trip
        PersistenceManager.shared.setCurrentTripId(tripId: 0)
        
        //Plot end marker
        if let currentLocation = foregroundLocationManager.getCurrentLocation() {
             delegate?.plotMarker(title: Captions.endLocation, snippet: "", icon: #imageLiteral(resourceName: "floow_ic_pin_to"), location: currentLocation.toCLLocationCoordinate2D(), isStartMarker: false)
        }
       
        //Set previous location to nil
        self.previousLocation = nil
    }
    
    func recenterToUserLocation() {
        if let currentLocation = foregroundLocationManager.getCurrentLocation() {
             delegate?.moveToLocation(location: currentLocation.toCLLocationCoordinate2D())
        }
    }
    
    func insertLocationDetail(currentLocation: CLLocation) {
        // Encrypt location before save it into database
        let encyptedLat = encryptCoordinate(coordinate: currentLocation.coordinate.latitude)
        
        let encyptedLng = encryptCoordinate(coordinate: currentLocation.coordinate.longitude)
        
        databaseManager.insertTripDetail(tripId: tripId, lat: encyptedLat, lng: encyptedLng)
    }
    
    func encryptCoordinate(coordinate: Double) -> String {
        let encrypted = try! String(coordinate).aesEncrypt(key: Security.key, iv: Security.iv)
        
        return encrypted
    }
    
    func tryResumeTrip() {
        let tripStatus = PersistenceManager.shared.getCurrentTripStatus()
        if tripStatus == .complated || tripStatus == .notAvailable  {
            return
        }
        
        self.tripId = PersistenceManager.shared.getCurrentTripId()
        let tripDetails = databaseManager.getTripDetails(tripId: self.tripId)
        for tripDetail in tripDetails {
            let location = decryptLocation(lat: tripDetail.lat, lng: tripDetail.lng)
            tripLocations.append(location)
        }
        
        //Set previous location to last location
        self.previousLocation = decryptLocation(lat: tripDetails[tripDetails.count - 1].lat, lng: tripDetails[tripDetails.count - 1].lng).toCLLocation()
        
        delegate?.initUI()
        
        //Resume Trip
        foregroundLocationManager.startListening()
    }
}

//MARK: Callbacks

extension MainViewModel: ForegroundLocationManagerDelegate {
    func userDidMove(location: CLLocation) {
        if let previousLocation = self.previousLocation {
            delegate?.userDidMove(previousLocation: previousLocation.toCLLocationCoordinate2D(), headLocation: location.toCLLocationCoordinate2D())
        } else {
            delegate?.plotMarker(title: Captions.startLocation, snippet: "", icon: #imageLiteral(resourceName: "floow_ic_pin_from"), location: location.toCLLocationCoordinate2D(), isStartMarker: true)
        }
        self.previousLocation = location
        insertLocationDetail(currentLocation: location)
    }
    
    func moveToUserLocation() {
        if let currentLocation = foregroundLocationManager.getCurrentLocation() {
            delegate?.moveToLocation(location: currentLocation.toCLLocationCoordinate2D())
        }
    }
}
