//
//  BaseViewModel.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 06/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation
import CoreLocation

class BaseViewModel: NSObject {
    
    func decryptLocation(lat: String, lng: String) -> CLLocationCoordinate2D {
        do {
            let decryptLat = Double(try lat.aesDecrypt(key: Security.key, iv: Security.iv)) ?? 0
            let decryptLng = Double(try lng.aesDecrypt(key: Security.key, iv: Security.iv)) ?? 0
            
            return CLLocationCoordinate2D(latitude: decryptLat, longitude: Double(decryptLng))
        } catch {
            return CLLocationCoordinate2D(latitude: 0, longitude: 0)
        }
    }
    
}
