//
//  TripsHistoryViewModel.swift
//  TrackUserLocationTask
//
//  Created by Husam Abuhajjaj on 03/03/2020.
//  Copyright © 2020 Husam Abuhajjaj. All rights reserved.
//

import Foundation

protocol TripsHistoryDelegate: class {
    func reloadTable()
    func goToTripDetailController(trip: Trip)
}

class TripsHistoryViewModel: NSObject {
    var trips: [Trip] = []
    let databaseManager = DatabaseManager()
    //weak to avoid retain cycle
    weak var delegate: TripsHistoryDelegate?
    
    func getTrips() {
        trips = databaseManager.getAllTrips()
        delegate?.reloadTable()
    }
    
    //Callback from cell
    
    func viewTrip(trip: Trip) {
        delegate?.goToTripDetailController(trip: trip)
    }
    
    func deleteTrip(tripId: Int) {
        databaseManager.deleteTripById(id: tripId)
        getTrips()
        delegate?.reloadTable()
    }
}
